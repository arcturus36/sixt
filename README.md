# Sixt Hiring Test


### Problem 1 - Anagrams
Implement the logic to detect a pair of anagrams.
An anagram is direct word switch or word play,
the result of rearranging the letters of a word or phrase to produce a new word or phrase,
using all the original letters exactly once; for example, the word �anagram' can be rearranged into �nagaram�


### Problem 2 - Script Runner
Let's say we have a database of scripts:
each script has an arbitrary number of dependencies.
The dependencies are expressed as a list of scriptIds that need to be executed before a given script.
We want to come up with an execution plan so that we can run all of the scripts in a sane order.
The script is represented by: /src/main/java/p2/ScriptRunner.java

#

Once you are done with the task, please inform us over email
