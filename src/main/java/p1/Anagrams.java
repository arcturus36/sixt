package p1;

import static java.util.Objects.isNull;

public class Anagrams {

    public boolean check(String left, String right) {

        if (isNull(left) || isNull(right)) {
            return false;
        }

        if (left.isEmpty() || right.isEmpty()) {
            return false;
        }

        int leftValue = left.toLowerCase().chars().sum();
        int rightValue = right.toLowerCase().chars().sum();

        return leftValue == rightValue;
    }
}
