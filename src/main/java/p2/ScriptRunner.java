package p2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import static java.lang.String.*;
import static java.util.Comparator.comparingInt;
import static java.util.Objects.isNull;

public class ScriptRunner {

    public int[] runOrder(Script[] allScripts) {

        verifyArgs(allScripts);
        sortScriptsById(allScripts);

        List<Integer> resolved = new ArrayList<>();
        List<Integer> unResolved = new ArrayList<>();

        resolve(allScripts[0], allScripts, resolved, unResolved);

        return toIntArray(resolved);
    }

    private void verifyArgs(Script[] allScripts) {
        checkNotNull(allScripts);
        checkDuplicates(allScripts);
    }

    public void sortScriptsById(Script[] allScripts) {
        Arrays.sort(allScripts,comparingInt(Script::getScriptId));
    }

    private void resolve(Script script, Script[] allScripts, List<Integer> resolved, List<Integer> unResolved) {
        Integer scriptId = script.getScriptId();
        unResolved.add(scriptId);

        for (Integer id : script.getDependencies()) {
            if (isNotResolved(id, resolved)) {
                if (unResolved.contains(id)) {
                    throw new IllegalArgumentException(format("Circular dependencies detected between scripts %d and %d",id,script.getScriptId()));
                }
                resolve(allScripts[id], allScripts, resolved, unResolved);
            }
        }

        resolved.add(scriptId);
        unResolved.remove(scriptId);
    }

    private int[] toIntArray(List<Integer> resolved) {
        return resolved.stream().mapToInt(Integer::intValue).toArray();
    }

    private void checkNotNull(Script[] allScripts) {
        if (isNull(allScripts)) {
            throw new IllegalArgumentException("All scripts cannot be missing");
        }

        if (streamify(allScripts).anyMatch(Objects::isNull)) {
            throw new IllegalArgumentException("An script is missing");
        }
    }

    private void checkDuplicates(Script[] allScripts) {
        if (hasDuplicateIds(allScripts)) {
            throw new IllegalArgumentException("Duplicated script id");
        }
    }

    private boolean isNotResolved(Integer id, List<Integer> resolved) {
        return !resolved.contains(id);
    }

    private Stream<Script> streamify(Script[] allScripts) {
        return Stream.of(allScripts);
    }

    private boolean hasDuplicateIds(Script[] allScripts) {
        return streamify(allScripts).mapToInt(Script::getScriptId).distinct().count() != allScripts.length;
    }
}
