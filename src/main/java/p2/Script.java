package p2;

import java.util.List;

import static java.util.Objects.isNull;

public class Script {

    private final int scriptId;
    private final List<Integer> dependencies;

    public Script(int scriptId, List<Integer> dependencies) {
        this.scriptId = scriptId;
        this.dependencies = dependencies;
        verify();
    }

    private void verify() {

        if (isNull(dependencies)) {
            throw new IllegalArgumentException("Script dependencies cannot be null");
        }

        if (dependencies.contains(scriptId)) {
            throw new IllegalArgumentException("Script cannot have itself as dependency");
        }

        if (0 > scriptId) {
            throw new IllegalArgumentException("Script id cannot be less than zero");
        }

        if (dependencies.stream().anyMatch(this::isLessThanZero)) {
            throw new IllegalArgumentException("Dependency id cannot be less than zero");
        }
    }

    private boolean isLessThanZero(int scriptId) {
        return 0 > scriptId;
    }

    int getScriptId() {
        return scriptId;
    }

    List<Integer> getDependencies() {
        return dependencies;
    }
}