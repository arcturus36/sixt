package p2;

import org.junit.jupiter.api.Test;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

class ScriptTest {

    @Test void
    fail_when_script_dependencies_is_missing() {
        Throwable throwable = catchThrowable(() -> new Script(1,null));

        assertThat(throwable)
                .isExactlyInstanceOf(IllegalArgumentException.class)
                .hasMessage("Script dependencies cannot be null");
    }

    @Test void
    fail_when_script_is_dependency_of_itself() {
        Throwable throwable = catchThrowable(() -> new Script(1, singletonList(1)));

        assertThat(throwable)
                .isExactlyInstanceOf(IllegalArgumentException.class)
                .hasMessage("Script cannot have itself as dependency");
    }

    @Test void
    fail_when_script_id_is_less_than_zero() {
        Throwable throwable = catchThrowable(() -> new Script(-1, singletonList(2)));

        assertThat(throwable)
                .isExactlyInstanceOf(IllegalArgumentException.class)
                .hasMessage("Script id cannot be less than zero");
    }

    @Test void
    fail_when_dependency_id_less_than_zero() {
        Throwable throwable = catchThrowable(() -> new Script(0, singletonList(-1)));

        assertThat(throwable)
                .isExactlyInstanceOf(IllegalArgumentException.class)
                .hasMessage("Dependency id cannot be less than zero");
    }
}
