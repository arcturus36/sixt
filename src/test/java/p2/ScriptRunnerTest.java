package p2;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;


class ScriptRunnerTest {

    private ScriptRunner scriptRunner;

    @BeforeEach void
    init() {
        scriptRunner = new ScriptRunner();
    }

    @Test void
    resolve_script_dependencies() {

        Script[] allScripts = new Script[]{
                new Script(0, asList(1, 2)),
                new Script(1, asList(3,5)),
                new Script(2, singletonList(4)),
                new Script(3, emptyList()),
                new Script(4, singletonList(5)),
                new Script(5, emptyList())
        };

        assertArrayEquals(new int[]{3, 5, 1, 4, 2, 0}, scriptRunner.runOrder(allScripts));
    }

    @Test void
    sort_scripts_by_id() {

        Script[] allScripts = new Script[3];

        allScripts[2] = new Script(0, emptyList());
        allScripts[0] = new Script(1, emptyList());
        allScripts[1] = new Script(2, emptyList());

        Script[] expected = new Script[3];

        expected[0] = new Script(0, emptyList());
        expected[1] = new Script(1, emptyList());
        expected[2] = new Script(2, emptyList());

        scriptRunner.sortScriptsById(allScripts);

        assertThat(expected)
                .usingFieldByFieldElementComparator()
                .isEqualTo(allScripts);
    }

    @Test void
    fail_when_circular_dependencies_detected() {
        Script[] allScripts = new Script[]{
                new Script(0, singletonList(1)),
                new Script(1, singletonList(0)),
        };

        Throwable throwable = catchThrowable(() -> scriptRunner.runOrder(allScripts));

        assertThat(throwable)
                .isExactlyInstanceOf(IllegalArgumentException.class)
                .hasMessage("Circular dependencies detected between scripts 0 and 1");
    }

    @Test void
    fail_when_duplicated_script_id() {
        Script[] allScripts = new Script[]{
                new Script(1, emptyList()),
                new Script(1, emptyList())
        };

        Throwable throwable = catchThrowable(() -> scriptRunner.runOrder(allScripts));

        assertThat(throwable)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Duplicated script id");
    }

    @Test void
    fail_when_all_scripts_are_missing() {
        Throwable throwable = catchThrowable(() -> scriptRunner.runOrder(null));

        assertThat(throwable)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("All scripts cannot be missing");
    }

    @Test void
    fail_when_an_script_is_missing() {
        Throwable throwable = catchThrowable(() -> scriptRunner.runOrder(new Script[]{null}));

        assertThat(throwable)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("An script is missing");
    }
}