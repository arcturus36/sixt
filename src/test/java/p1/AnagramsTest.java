package p1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AnagramsTest {

    private Anagrams anagrams;

    @BeforeEach
    void init() {
        anagrams = new Anagrams();
    }

    @Test void
    given_two_words_with_same_letters_then_is_an_anagram() {
        assertTrue(anagrams.check("german","manger"));
    }

    @Test void
    given_two_words_with_different_letters_then_is_not_an_anagram() {
        assertFalse(anagrams.check("german","mangerr"));
    }

    @Test void
    is_an_anagram_when_left_word_has_upper_case() {
        assertTrue(anagrams.check("German","manger"));
    }

    @Test void
    is_an_anagram_when_right_word_has_upper_case() {
        assertTrue(anagrams.check("german","Manger"));
    }

    @Test void
    not_an_angram_when_left_word_is_blank() {
        assertFalse(anagrams.check("", "manger"));
    }

    @Test void
    not_an_angram_when_right_word_is_blank() {
        assertFalse(anagrams.check("german", ""));
    }

    @Test void
    not_an_angram_when_left_word_is_missing() {
        assertFalse(anagrams.check(null, "manger"));
    }

    @Test void
    not_an_angram_when_right_word_is_missing() {
        assertFalse(anagrams.check("german", null));
    }
}